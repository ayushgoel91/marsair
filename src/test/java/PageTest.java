import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class PageTest {
    WebDriver driver;
    AutomatedTests searchTest;

    @BeforeSuite
    public void initializeVars(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        driver = new ChromeDriver();
        driver.get(Constants.TEST_URL);
        searchTest = new AutomatedTests();
    }

    @Test(description = "Test when invalid months are given")
    public void searchFlightInvalidMonth() {
        String expected = "Search Results\n" +
                "Unfortunately, this schedule is not possible. Please try again.\n" +
                "Back";
        String actual = searchTest.searchFlight(driver, "July", "December");
        Assert.assertEquals(expected, actual);
    }

    // Raised this as bug but this is only test data found to automate
    @Test(description = "Test when invalid months are given and flights are available")
    public void searchFlightValidMonth() {
        String expected = "Search Results\n" +
                "Seats available!\n" +
                "Call now on 0800 MARSAIR to book!\n" +
                "Back";
        String actual = searchTest.searchFlight(driver, "July", "December (two years from now)");
        Assert.assertEquals(expected, actual);
    }

    @AfterMethod
    public void returnHome(){
        searchTest.refreshHome(driver);
    }

    @AfterSuite
    public void closeDriver(){
        driver.close();
    }
}
