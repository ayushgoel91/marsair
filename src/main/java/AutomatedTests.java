import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AutomatedTests {

    public String searchFlight(WebDriver driver, String departure, String arrival){
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("departing")));
        Select departureElem = new Select(driver.findElement(By.id("departing")));
        Select arrivalElem = new Select(driver.findElement(By.id("returning")));
        departureElem.selectByVisibleText(departure);
        arrivalElem.selectByVisibleText(arrival);

        driver.findElement(By.xpath("//*[@id=\"content\"]/form/dl[4]/dd/input")).click();
        return driver.findElement(By.id("content")).getText();
    }

    public void refreshHome(WebDriver driver) {
        driver.findElement(By.linkText("MarsAir")).click();
    }
}
